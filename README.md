# [T3chFlicks - Smart Buoy](https://t3chflicks.com/shop/kit/smart-buoy)


This repo stores the code for the Smart Buoy Project.
Check out the blog post at https://t3chflicks.com/shop/kit/smart-buoy

![gif_of_buoy](./smart_buoy_thumbnail_square.gif)


* `buoy/` contains the code which runs on an Arduino inside the Buoy, as well as the build files (3d model, schematic etc).

* `client/` contains the code for the dashboard which is a VueJS application

* `server/` contains the code for the Flask backend which runs the websockets and database queries as well as generic server stuff.


<a href="http://www.youtube.com/watch?feature=player_embedded&v=S-XMT6GDWk8
" target="_blank"><img src="http://img.youtube.com/vi/S-XMT6GDWk8/0.jpg"
alt="Smart Buoy Summary YouTube Video" width="240" height="180" /></a>
